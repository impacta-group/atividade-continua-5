# Atividade Contínua 5

## Funcionamento
> Se trata de um codigo Flask baseado em quatro rotas de API, abaixo estão descritos as chamadas e suas respectivas funções:

`/atividadecontinuacinco`
- A rota /atividadecontinuacinco representa a página principal da API. Ao ser requisitada, deve-se retornar o status code 200, indicando que a requisição foi bem-sucedida.

`/atividaddecontinuacinco/integrantes`
- A rota /integrantes contém a lista dos membros da equipe de desenvolvimento da atividade e seus respectivos RA's. Ao ser requisitada, deve-se retornar o status code 200, indicando que a requisição foi bem-sucedida

`/atividadecontinuacinco/nasa`
- A rota /atividadecontinuacinco/nasa é responsável por receber os dados da API pública da NASA, que tem como funcionalidade consultar informações sobre asteroides no espaço. Ao requisitá-la, deve-se retornar o status code 200, indicando que a requisição foi bem-sucedida.

`/atividadecontinuacinco/livro`
- A rota /atividadecontinuacinco/livro contém a lista dos livros e autores favoritos dos membros da equipe de desenvolvimento da atividade. Ao ser requisitada, deve-se retornar o status code 200, indicando que a requisição foi bem-sucedida.

`/`
- A rota / é a rota padrão do index, responsável por redirecionar para a rota /atividadecontinuacinco, a qual adotou a função de página principal (home). Ao ser requisitada, deve-se retornar o status code 200, indicando que a requisição foi bem-sucedida.

Para o funcionamento da Pipeline, inicia-se executando o arquivo que contém o container Docker, o qual é composto por dois códigos essenciais: app.py e test.py. O primeiro código, já mencionado anteriormente, tem como finalidade habilitar o uso das URIs. O segundo código, denominado test.py, tem a função de realizar testes unitários para verificar e validar o funcionamento adequado das URIs. Após executar o container, é necessário aguardar o resultado dos testes unitários, os quais ao final da execução apresentarão a mensagem "OK", indicando que os testes foram concluídos com sucesso.

## Funcionamento com o Docker
> Pode-se observar que possuímos dois containers. Mesclados por um docker-compose. O primeiro container subirá a aplicação Flask, enquanto, o segundo fará os testes com o PyUnit.

## NASA API - Mudança de Key

Para adquirir uma key para API da NASA, Acesse: https://api.nasa.gov/. No informe de seus dados será retornado uma key para substituir a já utilizada no 'Environment.env'.

## Validação de funcionamento e testes unitarios

Foi realizado o seguinte procedimento:
- Subiu-se uma instância EC2 na AWS;
- Instalou-se o Docker na instância, com o objetivo de disponibilizar um ambiente isolado e padronizado para a execução da aplicação;
- Adotou-se o mesmo padrão de uso de comandos do Linux, a fim de garantir a familiaridade para os usuários;
- Foi realizado o processo de validação dos testes para garantir o funcionamento correto da aplicação.
- Adicionamos validação de segurança do Semgrep nas URLs que o container com Flask subiu.


## Integrantes do Grupo

- Gustavo Rodrigues Lima RA: 2201436
- Kauã Barbosa do Nascimento RA: 2201260

 Observação: Caso apresentado um user chamado `Ubuntu` foi por que fizemos algumas mudanças pela EC2, utilizando o Vim e mesmo vinculando um usuário subiu como `Ubuntu`


