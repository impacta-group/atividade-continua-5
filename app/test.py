import unittest
import requests
from app import app

class TestFlaskApp(unittest.TestCase):
    def setUp(self):
        self.app = app.test_client()

    # verificando se o index '/atividadecontinuacinco' está funcionando
    def test_index(self):
        response = self.app.get('/atividadecontinuacinco')
        self.assertEqual(response.status_code, 200)

    # verificando se a exibição de nomes e ra's do integrantes está funcional
    def test_integrantes(self):
        response = self.app.get('/atividadecontinuacinco/integrantes')
        self.assertEqual(response.status_code, 200)

    
    # verificando se a api pública e o nosso import de dados da mesma está ok.
    def test_api_nasa(self):
        response = self.app.get('/atividadecontinuacinco/nasa')
        self.assertEqual(response.status_code, 200)

    
    # verificando se nossos livros favoritos estão sendo retornados.
    def test_livros(self):
        response = self.app.get('/atividadecontinuacinco/livros')
        self.assertEqual(response.status_code, 200)

    # verificando se o '/' é corretamento_redirecionado
    def test_redirect(self):
        response = self.app.get('/')
        self.assertEqual(response.status_code, 301)
      
if __name__ == '__main__':
    unittest.main()