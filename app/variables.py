import os
from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv("../environment.env"))

key_nasa = os.environ.get("api_key")
