from flask import Flask, jsonify, redirect, make_response
from variables import key_nasa
from requests import get

app = Flask(__name__)

def consulta_api_nasa():
  url = f"https://api.nasa.gov/neo/rest/v1/neo/3542519?api_key={key_nasa}"
  resp = get(url)
  resp_formated = resp.json()
  return resp_formated

livros = [
    {
      'editora':'jambo',
      'nome':'A Lenda de Drizzt Vol. 1 - Patria'
    },
    {
      'editora':'jambo',
      'nome':'A Lenda de Drizzt Vol. 2 - Exilio'
    },
    {
      'editora':'jambo',
      'nome':'A Lenda de Drizzt Vol. 3 - Refugio'
    },
    {
      'editora':'draco',
      'nome':'Devorados'
    },
    {
      'editora':'camelot',
      'nome':'O Chamado de Cthulhu'
    },
    {
      'editora':'darkside',
      'nome':'H.P. Lovecraft - Medo Classico - Vol. 1 - Myskatonic Edition'
    },
    {
      'editora':'darkside',
      'nome':'H.P. Lovecraft - Medo Classico - Vol. 2 - Myskatonic Edition'
    }
]

integrantes = [
    {
        'integrante':1,
        'nome':'Gustavo Rodrigues Lima',
        'RA':'2201436'
    },
    {
        'integrante':2,
        'nome':'Kaua Barbosa do Nascimento',
        'RA':'2201260'
    },
    {
        'integrante':3,
        'nome':'Leonardo Bispo Andreata',
        'RA':'2201582'
    },
    {
        'integrante':4,
        'nome':'Gustavo Rossini Ozzetti',
        'RA':'2101054'
    },
    {
        'integrante':5,
        'nome':'Matheus Cardoso Morais',
        'RA':'2200764'
    }

]

@app.route('/')
def index():
    return redirect('/atividadecontinuacinco'), 301

@app.route('/atividadecontinuacinco')
def hello_world():
    return 'Esta é a página principal, você pode consultar as seguintes URIS: /intengrantes para verificar os integrantes do grupo, /nasa para puxar dados de uma api pública da nasa, e /livros para verificar os melhores livros segundo os integrantes do grupo.', 200

@app.route('/atividadecontinuacinco/integrantes')
def showintegrantes():
    return jsonify(integrantes), 200

@app.route('/atividadecontinuacinco/nasa')
def showreturnnasa():
    response = consulta_api_nasa()
    return jsonify(response), 200

@app.route('/atividadecontinuacinco/livros')
def showall():
    return jsonify(livros), 200

if __name__ == '__main__':
    app.run(debug=True, port=5000)